package singleton;

import models.MailerDataMO;

import java.util.ArrayList;

public class MailerCompleteInfoSingleton {

    private static MailerCompleteInfoSingleton mailerCompleteInfoSingleton = null;
    private ArrayList<MailerDataMO> mailerDataMOList = null;

    private MailerCompleteInfoSingleton() {

    }

    public static MailerCompleteInfoSingleton getInstance() {
        if (mailerCompleteInfoSingleton == null)
            mailerCompleteInfoSingleton = new MailerCompleteInfoSingleton();
        return mailerCompleteInfoSingleton;
    }

    public ArrayList<MailerDataMO> getMailerDataMOList() {
        return mailerDataMOList;
    }

    public void setMailerDataMOList(ArrayList<MailerDataMO> mailerDataMOList) {
        this.mailerDataMOList = mailerDataMOList;
    }

    public void cleanMailerDataList() {
        if (mailerDataMOList != null && mailerDataMOList.size() > 0) {
            mailerDataMOList.clear();
        }
    }
}
