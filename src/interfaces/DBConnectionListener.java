package interfaces;

import java.sql.Statement;

public interface DBConnectionListener {
    void onConnectionSuccess(Statement statement);

    void onConnectionFailed();
}
