package interfaces;

public interface MailSentListener {
    void onMailSentSuccessfully();

    void onMailFailure();
}
