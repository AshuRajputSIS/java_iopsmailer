package interfaces;

import java.util.Map;

public interface QueryExecuteListener {
    void onQueryExecutionSuccess(Map<String, Object[]> queryResult, int queryPosition);

    void onQueryExecutionFailed();
}
