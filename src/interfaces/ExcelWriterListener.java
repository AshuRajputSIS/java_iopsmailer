package interfaces;

public interface ExcelWriterListener {

    void onWrittenSuccessfully(int filePosition);

    void onWrittenFailure();

}
