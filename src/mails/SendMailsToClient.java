package mails;

import interfaces.MailSentListener;
import models.MailerDataMO;
import singleton.MailerCompleteInfoSingleton;
import utility.MailerConfig;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.util.Properties;

public class SendMailsToClient {

    private MailSentListener mailSentListener = null;

    public SendMailsToClient(MailSentListener mailSentListener) {
        this.mailSentListener = mailSentListener;
    }

    public void sendEmailViaGmail() {
        final String username = "fcpaybill@gmail.com";
        final String password = "ashu@28jan90";

        /*Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");*/


        /*Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", username);
        props.put("mail.smtp.password", password);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");*/


        Properties props = new Properties();
        props.put("mail.smtp.user", username);
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "465");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.debug", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {
            String to = "ashurajput11@gmail.com";
//            String from = "ashu.rajput@sisindia.com";
            String from = "fcpaybill@gmail.com";
            String subject = "Subject : Sending first mail";

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            message.setSubject(subject);
            message.setText("Dear Sir,\n\nWe are happy to announce that you are our valuable customer\n\nThanks\n\nAshu Rajput");

//            Transport.send(message);

            Transport transport = session.getTransport("smtps");
            transport.connect("smtp.gmail.com", 465, username, password);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();

            System.out.println("Done");
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    public void sendEmailViaSISMailServer(int filePosition) {
        final String userAndFrom = "iops.report@sisindia.com";
        final String password = "Letmein@123";
        final int port = 366; //366,465
        final String host = "mail.sisindia.com";

        Properties props = new Properties();
//        props.put("mail.smtp.user", userAndFrom);
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", "" + port);
//        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.startssl.enable", "false");
        props.put("mail.smtp.ssl.enable", "false");
//        props.put("mail.smtp.debug", "true");
        props.put("mail.smtp.auth", "true");
//        props.put("mail.smtp.socketFactory.port", ""+port);
//        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//        props.put("mail.smtp.socketFactory.fallback", "true");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(userAndFrom, password);
                    }
                });

        try {
            String to = "ashurajput11@gmail.com";
            String cc = "lalit.kumar1@sisindia.com";
            String subject = "Subject : Sending mail with attachment";

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(userAndFrom));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(cc));
            message.setSubject(subject);
            message.setText("Dear Sir,\n\nWe are happy to announce that you are our one of the lucky customer.\n\n" +
                    "Please find the attachment winner policy for your reference \n\nThanks\n\nBabu Bhaiya");

            attachSingleFileInMail(message, filePosition);
            message.saveChanges();

            Transport transport = session.getTransport("smtp");
            transport.connect(host, port, userAndFrom, password);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();

            mailSentListener.onMailSentSuccessfully();

        } catch (Exception e) {
            e.printStackTrace();
            mailSentListener.onMailFailure();
        }
    }

    public void attachMultipleFileInMail(Message message) {

        try {
            MimeBodyPart messageBodyPart;

            Multipart multipart = new MimeMultipart();
            messageBodyPart = new MimeBodyPart();

            //Getting multiple files from particular folder
            File folder = new File(MailerConfig.FILE_STORAGE_PATH);
            File[] listOfFiles = folder.listFiles();

            for (File file : listOfFiles) {
                if (file.isFile()) {
                    System.out.println(file.getName());

//                    String file = "D:\\DDriveData\\JavaLibsOrJarFiles\\SamplesFiles\\Mails.xls";
//                    String fileName = "Mails.xls";
                    DataSource source = new FileDataSource(file);
                    messageBodyPart.setDataHandler(new DataHandler(source));
                    messageBodyPart.setFileName(file.getName());
                    multipart.addBodyPart(messageBodyPart);
                    message.setContent(multipart);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void attachSingleFileInMail(Message message, int filePosition) {

        try {
            MimeBodyPart messageBodyPart;

            Multipart multipart = new MimeMultipart();
            messageBodyPart = new MimeBodyPart();

            MailerDataMO receivedMO = MailerCompleteInfoSingleton.getInstance().getMailerDataMOList().get(filePosition);

            DataSource source = new FileDataSource(MailerConfig.FILE_STORAGE_PATH + receivedMO.getExcelName() + ".xls");
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(receivedMO.getExcelName() + ".xls");
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
