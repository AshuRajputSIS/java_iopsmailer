package repositories;

import interfaces.DBConnectionListener;
import utility.MailerConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DBConnectionBuilder {

    private DBConnectionListener dbConnectionListener = null;
    static Statement stmt = null;
    private Connection conn = null;

    public DBConnectionBuilder(DBConnectionListener dbConnectionListener) {
        this.dbConnectionListener = dbConnectionListener;
//        createDBConnection();
    }

    public Statement getDBConnection() {
        if (stmt == null && conn == null) {
            try {
                //STEP 2: Register JDBC driver
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

                //STEP 3: Open a connection
                System.out.println("Connecting to database...");
                conn = DriverManager.getConnection(MailerConfig.DB_URL, MailerConfig.DB_USER, MailerConfig.DB_PASS);

                //STEP 4: Execute a query
                stmt = conn.createStatement();

           /* MailerEntry object = new MailerEntry();
            //Using below method to execute query- to fetch data from DB after successful connection
//            object.getEmployeeDataQuery();
            object.triggerMailSender();*/

            } catch (SQLException se) {
                se.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            } /*finally {
                try {
                    if (stmt != null)
                        stmt.close();
                } catch (SQLException se2) {
                }
                try {
                    if (conn != null)
                        conn.close();
                } catch (SQLException se) {
                    se.printStackTrace();
                }
            }*/

        }
        return stmt;
    }

    public void createDBConnection() {
        if (stmt == null && conn == null) {
            try {
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                System.out.println("Connecting to database...");
                conn = DriverManager.getConnection(MailerConfig.DB_URL, MailerConfig.DB_USER, MailerConfig.DB_PASS);
                stmt = conn.createStatement();
                dbConnectionListener.onConnectionSuccess(stmt);
            } catch (SQLException se) {
                se.printStackTrace();
                dbConnectionListener.onConnectionFailed();
            } catch (Exception e) {
                e.printStackTrace();
                dbConnectionListener.onConnectionFailed();
            }
        } else {
            dbConnectionListener.onConnectionSuccess(stmt);
        }
    }

    public void closeDBConnection() {
        if (stmt != null && conn != null) {
            try {
                stmt.close();
                conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
