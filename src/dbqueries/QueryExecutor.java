package dbqueries;

import interfaces.QueryExecuteListener;
import models.EmployeeMO;
import models.MailerDataMO;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class QueryExecutor {

    private Statement stmt = null;
    private QueryExecuteListener queryExecuteListener = null;

    public QueryExecutor(Statement stmt, QueryExecuteListener queryExecuteListener) {
        this.stmt = stmt;
        this.queryExecuteListener = queryExecuteListener;
    }

    public void getEmployeeDataQuery(int position) {

        new Thread(() -> {
            Map<String, Object[]> data = null;
            try {
                String SQL = "select top 15 * from Employee";
                ResultSet rs = stmt.executeQuery(SQL);
                // Iterate through the data in the result set and display it.
                data = new LinkedHashMap<>();

                ResultSetMetaData rsmd = rs.getMetaData();
                int columnLength = rsmd.getColumnCount();

                try {
                    Thread.sleep(600);
                } catch (Exception e) {
                }

                Object[] tableColumn = new Object[columnLength];

                for (int i = 0; i < columnLength; i++) {
                    tableColumn[i] = rsmd.getColumnName(i + 1);
                }

//            data.put("0", new Object[]{"FirstName", "EmployeeNo", "ContactNo"});
                data.put("0", tableColumn);
                int counterPosition = 1;
                while (rs.next()) {
                /*Object[] dataBinder = new Object[]{
                        rs.getString("FirstName"),
                        rs.getString("EmployeeNo"),
                        rs.getString("ContactNo")};*/

                    Object[] dataBinder = new Object[columnLength];

                    for (int i = 0; i < columnLength; i++) {
                        dataBinder[i] = rs.getString(rsmd.getColumnName(i + 1));
                    }

                    data.put("" + counterPosition, dataBinder);
                    counterPosition++;
                }
                queryExecuteListener.onQueryExecutionSuccess(data, position);
            }
            // Handle any errors that may have occurred.
            catch (SQLException e) {
                e.printStackTrace();
                queryExecuteListener.onQueryExecutionFailed();
            }

        }).start();

    }

    public synchronized void getInformationFromGenericQuery(MailerDataMO queryInfoMO, int queryPosition) {

        new Thread(() -> {
            Map<String, Object[]> data = null;

            try {
                ResultSet rs = stmt.executeQuery(queryInfoMO.getQuery());
                data = new LinkedHashMap<>();

                ResultSetMetaData rsmd = rs.getMetaData();
                int columnLength = rsmd.getColumnCount();

                try {
                    Thread.sleep(600);
                } catch (Exception e) {
                }

                Object[] tableColumn = new Object[columnLength];

                for (int i = 0; i < columnLength; i++) {
                    tableColumn[i] = rsmd.getColumnName(i + 1);
                }

                data.put("0", tableColumn);
                int counterPosition = 1;
                while (rs.next()) {

                    Object[] dataBinder = new Object[columnLength];
                    for (int i = 0; i < columnLength; i++) {
                        dataBinder[i] = rs.getString(rsmd.getColumnName(i + 1));
                    }
                    data.put("" + counterPosition, dataBinder);
                    counterPosition++;
                }
                queryExecuteListener.onQueryExecutionSuccess(data, queryPosition);
            } catch (SQLException e) {
                e.printStackTrace();
                queryExecuteListener.onQueryExecutionFailed();
            }
        }).start();
    }

    public void getEmployeeDetailsFromQuery() {
        try {
            String SQL = "select top 10 * from Employee";
            ResultSet rs = stmt.executeQuery(SQL);

            ArrayList<EmployeeMO> employeeMOArrayList = new ArrayList<>();

            EmployeeMO defaultMO = new EmployeeMO();
            defaultMO.setEmployeeFirstName("First Name");
            defaultMO.setEmployeeLastName("Second Name");
            defaultMO.setEmployeeContactNo("Contact No");
            employeeMOArrayList.add(defaultMO);

            while (rs.next()) {
                EmployeeMO actualValueMO = new EmployeeMO();
                actualValueMO.setEmployeeFirstName(rs.getString("FirstName"));
                actualValueMO.setEmployeeLastName(rs.getString("LastName"));
                actualValueMO.setEmployeeContactNo(rs.getString("ContactNo"));
                employeeMOArrayList.add(actualValueMO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
