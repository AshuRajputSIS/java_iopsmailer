import dbqueries.QueryExecutor;
import interfaces.DBConnectionListener;
import interfaces.ExcelWriterListener;
import interfaces.MailSentListener;
import interfaces.QueryExecuteListener;
import mails.SendMailsToClient;
import models.MailerDataMO;
import repositories.DBConnectionBuilder;
import singleton.MailerCompleteInfoSingleton;

import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;

public class MailExecutor implements DBConnectionListener, QueryExecuteListener, ExcelWriterListener, MailSentListener {

    private int queryLooperCounter = -1;
    private QueryExecutor queryExecutor = null;

    public static void main(String[] args) {
        MailExecutor executor = new MailExecutor();
        executor.createDemoData();
        DBConnectionBuilder connectionBuilder = new DBConnectionBuilder(executor);
        connectionBuilder.createDBConnection();
    }

    private void createDemoData() {

        ArrayList<MailerDataMO> preparedMailerDataList;

        //cleaning mailer data list if already exist for previous call
        MailerCompleteInfoSingleton.getInstance().cleanMailerDataList();

        String[] queryArray = new String[]{"select top 15 * from Employee",
                "select AppUserId,Sim1PhoneNo,AppVersion,IMEI1 from [dbo].[EmployeeDevice] where " +
                        "IsAOAppInstalled=1 and IsBlocked=0 and AppVersion is not NULL"};
        String[] excelName = new String[]{"EmployeeInfo", "DeviceInfo"};
        String[] excelSheetName = new String[]{"EmployeeDetails", "IOPSApp"};

        preparedMailerDataList = new ArrayList<>();

        for (int i = 0; i < queryArray.length; i++) {
            MailerDataMO model = new MailerDataMO();
            model.setQuery(queryArray[i]);
            model.setExcelName(excelName[i]);
            model.setExcelSheetName(excelSheetName[i]);
            preparedMailerDataList.add(model);
        }

        //Setting mailer data information for whole application
        MailerCompleteInfoSingleton.getInstance().setMailerDataMOList(preparedMailerDataList);
    }

    //-----------------------DB Connection call back--------------------------//
    @Override
    public void onConnectionSuccess(Statement statement) {
        System.out.println("Connection Done!!");
        if (statement != null) {
            queryExecutor = new QueryExecutor(statement, this);
//            queryExecutor.getEmployeeDataQuery();

            //Calling below method for '0' index value from mailer object list
//            queryLooperCounter = queryLooperCounter + 1;
            executeQueryLooper();
        }
    }

    @Override
    public void onConnectionFailed() {
        System.out.println("Connection Failed!!");
    }

    //-----------------------Query Executor call back--------------------------//
    @Override
    public void onQueryExecutionSuccess(Map<String, Object[]> queryResult, int queryPosition) {
        WriteToExcel writeToExcel = new WriteToExcel(this);
        MailerDataMO requestedMailerMO = MailerCompleteInfoSingleton.getInstance().getMailerDataMOList().get(queryPosition);
        writeToExcel.excelWriter2(queryResult, requestedMailerMO.getExcelSheetName(), requestedMailerMO.getExcelName(), queryPosition);
    }

    @Override
    public void onQueryExecutionFailed() {
        System.out.println("Query Execution---Failed!!");
        //Calling below method because [in case previous query execution failed need to continue with remaining query]
        executeQueryLooper();
    }

    //--------------------Writing data to excel, call back---------------------//
    @Override
    public void onWrittenSuccessfully(int filePosition) {

        System.out.println("Excel Written--Execution -- SUCCESS!!!");
        SendMailsToClient sendMail = new SendMailsToClient(this);
        sendMail.sendEmailViaSISMailServer(filePosition);
    }

    @Override
    public void onWrittenFailure() {
        System.out.println("Excel Written--Execution---Failed!!");

        //Calling below method because [in case previous excel writing execution failed need to continue with remaining excel creation]
        executeQueryLooper();
    }

    //-----------------------Mail Sent status call back-------------------------//
    @Override
    public void onMailSentSuccessfully() {
//        queryLooperCounter = queryLooperCounter + 1;
        System.out.println("Mail sent to user successfully....");
//        if (queryLooperCounter < MailerCompleteInfoSingleton.getInstance().getMailerDataMOList().size())
        executeQueryLooper();
    }

    @Override
    public void onMailFailure() {
        System.out.println("Unable to sent mail to user....");
        //Calling below method because [in case previous mail sent failed need to continue with remaining mail sending]
        executeQueryLooper();
    }

    private void executeQueryLooper() {

        ArrayList<MailerDataMO> preparedMailerDataList = MailerCompleteInfoSingleton.getInstance().getMailerDataMOList();

        queryLooperCounter = queryLooperCounter + 1;
        System.out.println("inside executeQueryLooper() with position " + queryLooperCounter + " ListSize " + preparedMailerDataList.size());

        if (queryLooperCounter < preparedMailerDataList.size()) {

            if (preparedMailerDataList != null && preparedMailerDataList.size() > 0) {
                /*for (byte b = 0; b < preparedMailerDataList.size(); b++) {
                    queryExecutor.getInformationFromGenericQuery(preparedMailerDataList.get(b), b);
                }*/
                queryExecutor.getInformationFromGenericQuery(preparedMailerDataList.get(queryLooperCounter), queryLooperCounter);
            }
        }
    }
}
