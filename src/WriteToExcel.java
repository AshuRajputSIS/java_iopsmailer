import interfaces.ExcelWriterListener;
import models.EmployeeMO;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import utility.MailerConfig;

import java.io.*;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/*import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;*/

public class WriteToExcel {

    private ExcelWriterListener excelWriterListener = null;

    public WriteToExcel(ExcelWriterListener excelWriterListener) {
        this.excelWriterListener = excelWriterListener;
    }

    public void excelWriter() {
        try {

            FileInputStream file = new FileInputStream(new File("D:\\DDriveData\\JavaLibsOrJarFiles\\SamplesFiles\\Mails.xls"));

            HSSFWorkbook workbook = new HSSFWorkbook(file);
            HSSFSheet sheet = workbook.getSheetAt(0);
            Cell cell = null;

            //Update the value of cell
            cell = sheet.getRow(1).getCell(2);
            cell.setCellValue(cell.getNumericCellValue() * 2);
            cell = sheet.getRow(2).getCell(2);
            cell.setCellValue(cell.getNumericCellValue() * 2);
            Row row = sheet.getRow(0);
            row.createCell(3).setCellValue("Value 2");

            file.close();

//            FileOutputStream outFile = new FileOutputStream(new File("C:\\update.xls"));
            FileOutputStream outFile = new FileOutputStream(new File("D:\\DDriveData\\JavaLibsOrJarFiles\\SamplesFiles\\Mails.xls"));
            workbook.write(outFile);
            outFile.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void excelWriter2(Map<String, Object[]> data, String excelSheetName, String fileName,int filePosition) {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet(excelSheetName);

        // Aqua background
        /*HSSFCellStyle style = workbook.createCellStyle();
        style.setFillBackgroundColor(HSSFColor.BRIGHT_GREEN.index);
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        cell.setCellStyle(style);*/

        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset) {
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);

                if (obj instanceof Date)
                    cell.setCellValue((Date) obj);
                else if (obj instanceof Boolean)
                    cell.setCellValue((Boolean) obj);
                else if (obj instanceof String)
                    cell.setCellValue((String) obj);
                else if (obj instanceof Double)
                    cell.setCellValue((Double) obj);
            }
        }

        try {
            FileOutputStream out = new FileOutputStream(new File(MailerConfig.FILE_STORAGE_PATH + fileName + ".xls"));
            workbook.write(out);
            out.close();
            System.out.println("Excel written successfully..");
            excelWriterListener.onWrittenSuccessfully(filePosition);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            excelWriterListener.onWrittenFailure();
        } catch (IOException e) {
            e.printStackTrace();
            excelWriterListener.onWrittenFailure();
        }
    }

    public void excelWriter3(ArrayList<EmployeeMO> employeeMOArrayList) {

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Employee Sheet");

        for (int i = 0; i < employeeMOArrayList.size(); i++) {
            Row row = sheet.createRow(i);

            EmployeeMO retrieveMO = employeeMOArrayList.get(i);
            Class objClass = retrieveMO.getClass();

            // Get the public methods associated with this class.
            Method[] methods = objClass.getMethods();
            for (int k = 0; k < methods.length; k++) {
                Cell cell = row.createCell(k);

                if (k == 0)
                    cell.setCellValue(retrieveMO.getEmployeeFirstName());
                else if (k == 1)
                    cell.setCellValue(retrieveMO.getEmployeeLastName());
                else if (k == 2)
                    cell.setCellValue(retrieveMO.getEmployeeContactNo());

                /*if (methods[k].equals(retrieveMO.getEmployeeFirstName()))
                    cell.setCellValue(retrieveMO.getEmployeeFirstName());
                else if (methods[k].equals(retrieveMO.getEmployeeLastName()))
                    cell.setCellValue(retrieveMO.getEmployeeLastName());
                else if (methods[k].equals(retrieveMO.getEmployeeContactNo()))
                    cell.setCellValue(retrieveMO.getEmployeeContactNo());*/
            }

            /*for (Method method : methods) {
                System.out.println("Public method found: " + method.toString());
            }*/

        }

        try {
            FileOutputStream out = new FileOutputStream(new File("D:\\DDriveData\\JavaLibsOrJarFiles\\SamplesFiles\\EmpDetails.xls"));
            workbook.write(out);
            out.close();
            System.out.println("Excel written successfully..");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
